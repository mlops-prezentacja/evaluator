
import argparse
import pandas as pd
from sklearn.metrics import f1_score, accuracy_score
import json

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--predictions", type=str, required=True, help="Path to train dataset")
    parser.add_argument("--reference", type=str, required=True, help="Path to train dataset")
    parser.add_argument("--target_column", type=str, required=True, help="Column with classname")
    parser.add_argument("--result_file", type=str, required=True, help="Path to result file (in json)")
    args = parser.parse_args()

    # Load train dataset
    predictions = pd.read_csv(args.predictions)[args.target_column]
    trues = pd.read_csv(args.reference)[args.target_column]

    f1 = f1_score(trues, predictions, average='macro')
    accuracy = accuracy_score(trues, predictions)

    with open(args.result_file, 'w') as f:
        json.dump({
            'accuracy': accuracy,
            'f1': f1
        }, f, indent=4)